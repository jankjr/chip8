import { CPU } from './cpu';
import maze from './roms/pong.bin';
const canvas = document.getElementById('canvas') as HTMLCanvasElement;
const ctx = canvas.getContext('2d');
ctx.imageSmoothingEnabled = false;
const cpu = new CPU();
cpu.initialize();
cpu.load(maze);
var iData = new ImageData(cpu.gfx, 64, 32);
const step = () => {
  for (let i = 0 ; i < 15 ; i ++) {
    cpu.step();
  }

  requestAnimationFrame(step);
};

const draw = () => {
  if (cpu.drawFlag) {
    ctx.putImageData(iData, 0, 0);
    cpu.drawFlag = false;
  }
  requestAnimationFrame(draw);
};

const keymapping = {
  '1': 1,
  '2': 2,
  '3': 3,
  '4': 0xc,
  'q': 4,
  'w': 5,
  'e': 6,
  'r': 0xd,
  'a': 7,
  's': 8,
  'd': 9,
  'f': 0xe,
  'z': 0xa,
  'x': 0,
  'c': 0xb,
  'v': 0xf,
};
window.addEventListener('keypress', e => {
  if (!(e.key in keymapping)) {
    return;
  }
  const k = keymapping[e.key as never as keyof (typeof keymapping)] as number;
  cpu.key[k] = 1;
});
window.addEventListener('keyup', e => {
  if (!(e.key in keymapping)) {
    return;
  }
  const k = keymapping[e.key as never as keyof (typeof keymapping)] as number;
  cpu.key[k] = 0;
});
// step();
// draw();