import {MersenneTwister19937} from 'random-js';
const FONT_SET = [
  0xf0,
  0x90,
  0x90,
  0x90,
  0xf0,
  0x20,
  0x60,
  0x20,
  0x20,
  0x70,
  0xf0,
  0x10,
  0xf0,
  0x80,
  0xf0,
  0xf0,
  0x10,
  0xf0,
  0x10,
  0xf0,
  0x90,
  0x90,
  0xf0,
  0x10,
  0x10,
  0xf0,
  0x80,
  0xf0,
  0x10,
  0xf0,
  0xf0,
  0x80,
  0xf0,
  0x90,
  0xf0,
  0xf0,
  0x10,
  0x20,
  0x40,
  0x40,
  0xf0,
  0x90,
  0xf0,
  0x90,
  0xf0,
  0xf0,
  0x90,
  0xf0,
  0x10,
  0xf0,
  0xf0,
  0x90,
  0xf0,
  0x90,
  0x90,
  0xe0,
  0x90,
  0xe0,
  0x90,
  0xe0,
  0xf0,
  0x80,
  0x80,
  0x80,
  0xf0,
  0xe0,
  0x90,
  0x90,
  0x90,
  0xe0,
  0xf0,
  0x80,
  0xf0,
  0x80,
  0xf0,
  0xf0,
  0x80,
  0xf0,
  0x80,
  0x80
];
export class CPU {
  public rng: MersenneTwister19937;
  public gfx: Uint8Array = new Uint8ClampedArray(64 * 32 * 4);
  public drawFlag: boolean = false;
  public key: Uint16Array = new Uint16Array(16);
  private v: Uint8Array = new Uint8Array(16);
  private stack: Uint16Array = new Uint16Array(16);

  private sp: number = 0;
  private i: number = 0;
  private pc: number = 0;
  private delayTimer: number = 0;
  private soundTimer: number = 0;

  private memory: Uint8Array = new Uint8Array(4096);
  public initialize(): void {
    this.pc = 0x200;
    this.rng = MersenneTwister19937.seed(0);
    this.clearScreen();
    for (let i = 0; i < 80; ++i) {
      this.memory[i] = FONT_SET[i];
    }
  }

  public load(bin: ArrayBuffer): void {
    const data = new Uint8Array(bin);
    for (let i = 0 ; i < bin.byteLength ; i ++) {
      this.memory[i + 0x200] = data[i];
    }
  }
  public findFirstPressedKey() {
    for (let i = 0; i < 16; i++) {
      if (this.key[i] !== 0) {
        return i;
      }
    }
    return -1;
  }
  public clearScreen() {
    for (let i = 0 ; i < this.gfx.length ; i++) {
      this.gfx[i] = i % 4 === 3 ? 255 : 0;
    }
    this.drawFlag = true;
  }
  public step() {
    this.exec();
  }
  private exec(): void {
    const inst = ((this.memory[this.pc] << 8) & 0xff00) | this.memory[this.pc + 1];
    this.pc += 2;
    const x = (inst & 0x0f00) >>> 8;
    const y = (inst & 0x00f0) >>> 4;

    const opcode = inst & 0xf000;
    let acc: number;


    switch (opcode) {
      case 0x0000:
        switch (inst & 0x000f) {
          case 0x0000:
            this.clearScreen();
            break;
          case 0x000e:
            if (this.sp === 0) {
              throw new Error('Invalid RET');
            }
            this.pc = this.stack[this.sp - 1];
            this.sp -= 1;
            break;
          default:
            throw new Error('Unknown opcode ' + inst);
        }
        break;
      case 0x1000: // jmp
        this.pc = inst & 0x0fff;
        break;
      case 0x2000:
        this.stack[this.sp] = this.pc;
        this.sp += 1;
        this.pc = inst & 0x0fff;
        break;
      case 0x3000: // Skip next instruction if Vx = kk.
        if (this.v[x] === (inst & 0x00ff)) {
          this.pc += 2;
        }
        break;
      case 0x4000: // Skip next instruction if Vx /= kk.
        if (this.v[x] !== (inst & 0x00ff)) {
          this.pc += 2;
        }
        break;
      case 0x5000: // Skip next instruction if Vx = Vy.
        if (this.v[x] === this.v[y]) {
          this.pc += 2;
        }
        break;
      case 0x6000:
        // 6xkk - LD Vx, byte
        // Set Vx = kk.
        // The interpreter puts the value kk into register Vx.
        this.v[x] = inst & 0x00ff;
        break;
      case 0x7000:
        // 7xkk - ADD Vx, byte
        // Set Vx = Vx + kk.
        // Adds the value kk to the value of register Vx, then stores the result in Vx.
        this.v[x] = this.v[x] + (inst & 0x00ff);
        break;
      case 0x8000:
        switch (inst & 0x000f) {
          case 0x0000:
            this.v[x] = this.v[y];
            break;
          case 0x0001:
            this.v[x] = this.v[x] | this.v[y];
            break;
          case 0x0002:
            this.v[x] = this.v[x] & this.v[y];
            break;
          case 0x0003:
            this.v[x] = this.v[x] ^ this.v[y];
            break;
          case 0x0004:
            acc = this.v[x] + this.v[y];
            this.v[x] = acc & 0x00ff;
            this.v[0x000f] = acc > 0x00ff ? 1 : 0;
            break;
          case 0x0005:
            if (this.v[x] > this.v[y]) {
              this.v[x] = this.v[x] - this.v[y];
              this.v[0x000f] = 1;
            } else {
              this.v[x] = this.v[y] - this.v[x];
              this.v[0x000f] = 0;
            }
            break;
          case 0x0006:
            this.v[0x000f] = this.v[x] & 0x0001;
            this.v[x] = this.v[x] >>> 1;
            break;
          case 0x0007:
            if (this.v[y] > this.v[x]) {
              this.v[x] = this.v[y] - this.v[x];
              this.v[0x000f] = 1;
            } else {
              this.v[x] = this.v[x] - this.v[y];
              this.v[0x000f] = 0;
            }
            break;
          case 0x000e:
            this.v[0x000f] = this.v[x] & 0x0001;
            this.v[x] = this.v[x] << 1;
            break;
          default:
            throw new Error('Unknown instruction ' + inst);
        }
        break;
      case 0x9000:
        if (this.v[x] !== this.v[y]) {
          this.pc += 2;
        }
        break;
      case 0xa000:
        this.i = inst & 0x0fff;
        break;
      case 0xb000:
        this.pc = this.v[0] + (inst & 0x0fff);
        break;
      case 0xc000:
        this.v[x] = this.rand() & (inst & 0x00ff);
        break;
      case 0xd000:
        const height = inst & 0x000f;
        const xv = this.v[x];
        const yv = this.v[y];
        this.v[0x000f] = 0;
        for (let yline = 0; yline < height; yline++) {
          const pixel = this.memory[this.i + yline];
          for (let xline = 0; xline < 8; xline++) {
            if ((pixel & (0x80 >>> xline)) !== 0) {
              if (this.gfx[(xv + xline + (yv + yline) * 64) * 4] === 255) {
                this.gfx[(xv + xline + (yv + yline) * 64) * 4] = 0;
                this.gfx[(xv + xline + (yv + yline) * 64) * 4 + 1] = 0;
                this.gfx[(xv + xline + (yv + yline) * 64) * 4 + 2] = 0;
                this.gfx[(xv + xline + (yv + yline) * 64) * 4 + 3] = 255;
                this.v[0x000f] = 1;
              } else {
                this.gfx[(xv + xline + (yv + yline) * 64) * 4] = 255;
                this.gfx[(xv + xline + (yv + yline) * 64) * 4 + 1] = 255;
                this.gfx[(xv + xline + (yv + yline) * 64) * 4 + 2] = 255;
                this.gfx[(xv + xline + (yv + yline) * 64) * 4 + 3] = 255;
              }
            }
          }
        }
        this.drawFlag = true;
        break;
      case 0xe000:
        switch (inst & 0x00ff) {
          case 0x009e:
            if (this.key[this.v[x]] !== 0) {
              this.pc += 2;
            }
            break;
          case 0x00a1:
            if (this.key[this.v[x]] === 0) {
              this.pc += 2;
            }
            break;
          default:
            throw new Error('Unknown instruction');
        }
        break;
      case 0xf000:
        switch (inst & 0x00ff) {
          case 0x0007:
            this.v[x] = this.delayTimer;
            break;
          case 0x000a:
            const pressedKey = this.findFirstPressedKey();
            if (pressedKey !== -1) {
              this.v[x] = pressedKey;
              break;
            } else {
              this.pc -= 2;
              return;
            }
          case 0x0015:
            this.delayTimer = this.v[x];
            break;
          case 0x0018:
            this.soundTimer = this.v[x];
            break;
          case 0x001e:
            this.v[0x000f] = this.i + this.v[x] > 0x0fff ? 1 : 0;
            this.i = (this.i + this.v[x]) & 0x0fff;
            break;
          case 0x0029:
            this.i = this.v[x] * 5;
            break;
          case 0x0033:
            this.memory[this.i] = Math.floor(this.v[x] / 100);
            this.memory[this.i + 1] = Math.floor(this.v[x] / 10) % 10;
            this.memory[this.i + 2] = (this.v[x] % 100) % 10;
            break;
          case 0x0055:
            for (let i = 0; i <= x; i++) {
              this.memory[this.i + i] = this.v[i];
            }
            this.i += x;
            break;
          case 0x0065:
            for (let i = 0; i <= x; i++) {
              this.v[i] = this.memory[this.i + i];
            }
            this.i += x;
            break;
          default:
            throw new Error('Unknown opcode ' + inst.toString(16));
        }
        break;
      default:
        throw new Error('Unknown opcode ' + inst.toString(16));
    }
    if (this.delayTimer > 0) {
      this.delayTimer -= 1;
    }
    if (this.soundTimer > 0) {
      if (this.soundTimer === 1) {
        this.beep();
      }
      this.soundTimer -= 1;
    }
  }
  private rand(): number {
    return this.rng.next() % (0xff + 1);
  }
  private beep(): void {
    console.log('beep');
  }

}
